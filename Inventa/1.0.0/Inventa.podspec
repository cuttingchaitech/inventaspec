Pod::Spec.new do |s|
  s.name             = 'Inventa'
  s.version          = '1.0.0'
  s.summary          = 'Geo-location and proximity based communication by brand.'

  s.description      = <<-DESC
    'Geo-location and proximity based communication by brand significantly improves the relevance of marketing of product/services to the Consumer. Notifications for offers, promotions and other information based on matching user purchase preferences, delivered at the right place and time by Starbucks, significantly increase the probability of the Consumer walking into the Cafe and further generating sales/transactions for the Cafe by conversion of qualified walk-ins into shoppers.'
                       DESC

  s.homepage         = 'https://ohaiapp.com'
  s.license          = { :type => 'Apache License, Version 2.0', :file => 'LICENSE' }
  s.author           = { 'Salman Qureshi' => 'salman@ohaiapp.com' }
  s.source           = { :http => 'https://bitbucket.org/ohaisalman/inventaframework/downloads/inventa.zip' }

  s.platform = :ios
  s.ios.deployment_target = '10.0'
  #s.swift_version = '4.0'

#s.source_files  = '*', 'Inventa.framework/*'
#s.source_files = '../Inventa/Classes/**/*'
  
  # s.resource_bundles = {
  #   'Inventa' => ['Inventa/Assets/*.png']
  # }

 s.pod_target_xcconfig = { 'SWIFT_VERSION' => '3' }

#s.source_files  = 'Inventa/**.*'
# s.exclude_files = "Classes/Exclude"

#s.public_header_files = 'Inventa.framework/Headers/*.{h}'

  s.frameworks = 'UIKit', 'CoreLocation'
  s.ios.vendored_frameworks = 'Inventa.framework'
  #s.dependency 'Alamofire'
  #s.dependency 'RealmSwift'
end
