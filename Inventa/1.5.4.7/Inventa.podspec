#
# Be sure to run `pod lib lint Inventa.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
s.name             = 'Inventa'
s.version          = '1.5.4.7'
s.summary          = 'Geo-location and proximity based communication by brand.'

s.description      = <<-DESC
'Geo-location and proximity based communication by brand significantly improves the relevance of marketing of product/services to the Consumer. Notifications for offers, promotions and other information based on matching user purchase preferences, delivered at the right place and time by Starbucks, significantly increase the probability of the Consumer walking into the Cafe and further generating sales/transactions for the Cafe by conversion of qualified walk-ins into shoppers.'
DESC

s.homepage         = 'https://ohaiapp.com'
s.license          = { :type => 'Private License', :file => 'LICENSE' }
s.author           = { 'Salman Qureshi' => 'salman@cuttingchaitech.com' }
s.source           = { :http => 'https://bitbucket.org/cuttingchaitech/inventaframework/downloads/Inventa_v1_5_4_7.zip'}
#s.source           = { :git => 'https://heysalman@bitbucket.org/cuttingchaitech/inventa-ios-library.git',
#  :branch => 'IN-316-iosinventa-librarySwift5' }


s.social_media_url = 'https://twitter.com/ersalmanqureshi'

s.ios.deployment_target = '10.0'
s.swift_version = '5'
#s.source_files = 'Classes/**/*'

s.pod_target_xcconfig = {
    'SWIFT_VERSION' => '3.0'
}
s.frameworks = 'UIKit', 'CoreLocation', 'UserNotifications'

s.ios.vendored_frameworks = 'Inventa.framework'

s.dependency 'Alamofire', '~> 4.9.1'
s.dependency 'RealmSwift', '~> 3.20.0'
s.dependency 'RxSwift', '~> 4.2.0'
s.dependency 'Realm', '~> 3.20.0'
s.dependency 'RxAlamofire'
s.dependency 'Socket.IO-Client-Swift', '~> 15.0.0'
s.dependency 'AWSCore'
s.dependency 'AWSSNS'
s.dependency 'AWSCognito'

end


